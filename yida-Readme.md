## 宜搭APP后端服务
___
* 编译打包

` mvn clean package -Dmaven.test.skip=true `

* 配置文件

配置详情见 **src/main/resources/application.yml**

* 运行环境要求

需要正确安装并配置Jre环境

需要配置 **mysql数据库环境(本地配置即可)**、**redis环境**、**minio服务器**，
以上服务的配置信息(用户名、密码、地址等信息需要和**src/main/resources/application.yml**中的配置保持一致)

* 运行

`java -jar yida-0.0.1-SNAPSHOT.jar`

> 若需指定自定义的参数可在上述命令后追加相应格式的参数信息
>
>例如： java -jar yida-0.0.1-SNAPSHOT.jar --server.port=8081
>表示将服务端口号指定为 8081


## 宜搭APP前端服务
___
* 修改配置

`app/src/main/java/com/example/wearing/LoginActivity.java`中第56行修改数据库地址为IPV4地址

## 启动与运行
___
1. 安装并启动 **宜搭APP后端服务-运行环境要求** 中提到的各项服务，mysql需要导入 **yida.sql** 数据库脚本文件


2. 修改后端服务中配置与现有服务一致