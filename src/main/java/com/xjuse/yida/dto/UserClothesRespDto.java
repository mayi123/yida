package com.xjuse.yida.dto;

import com.xjuse.yida.entity.UserClothes;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Desc 查询用户衣物分类数据DTO
 * @Author 嗜雪的蚂蚁
 * @Date 2022/10/24 23:13
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserClothesRespDto {

    private LocalDateTime time;

    private List<UserTypeClothes> data;

    /***
     * @Description
     * @author 嗜雪的蚂蚁
     **/
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class UserTypeClothes {
        //衣物分类(名称)
        private String clothesType;
        private String clothesId;
        private Integer clothCount;
        private List<UserClothes> userClothesList;
    }
}
