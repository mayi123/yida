package com.xjuse.yida.dto;


import com.xjuse.yida.entity.WearRecommand;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户穿搭推荐 历史数据 DTO
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WearRecomHistoryDto {
    /**
     * 推荐时间
     */
    private LocalDateTime recomTime;
    /**
     * 推荐ID
     */
    private String recomId;
    /**
     * 推荐数据
     */
    private List<WearRecommand>  data = new ArrayList<>(4);
}
