package com.xjuse.yida.dto;


import com.xjuse.yida.entity.UserClothes;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户穿搭推荐数据DTO
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserClothRecomDto {

    private LocalDateTime time;

    private Map<String,UserClothes> data = new HashMap<>(4);
}
