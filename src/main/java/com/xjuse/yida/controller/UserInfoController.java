package com.xjuse.yida.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author duwx
 * @since 2022-10-22
 */
@RestController
@RequestMapping("/user-info")
public class UserInfoController {

}

