package com.xjuse.yida.controller;


import com.xjuse.yida.commom.jwt.JwtUser;
import com.xjuse.yida.commom.jwt.interceptor.AuthStorage;
import com.xjuse.yida.commom.util.Result;
import com.xjuse.yida.entity.UserClothes;
import com.xjuse.yida.service.UserClothesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户衣物服务 前端控制器
 * </p>
 *
 * @author duwx
 * @since 2022-10-21
 */
@RestController
@RequestMapping("/user-clothes")
@RequiredArgsConstructor
@Api(value = "用户衣物信息管理", tags = "用户衣物信息管理")
public class UserClothesController {

    private final UserClothesService userClothesService;

    @PostMapping("/addUserClothes")
    @ApiOperation("用户衣物信息录入")
    public Result addUserClothes(@RequestBody UserClothes userClothes) {
        return userClothesService.insertUserClothes(userClothes);
    }

    @GetMapping("/clothesByUser")
    @ApiOperation("按用户查找所有衣物")
    public Result findClothedByUser() {
        return userClothesService.findClothesByUser();
    }

}

