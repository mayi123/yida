package com.xjuse.yida.controller;


import com.xjuse.yida.commom.minio.config.MinioProperties;
import com.xjuse.yida.commom.minio.minio.MinioHandler;
import com.xjuse.yida.commom.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import sun.security.krb5.internal.crypto.RsaMd5CksumType;

import java.util.List;

/***
 * @Description  minio文件服务器Controller
 * @Date 16:12 2022/10/23
 * @author 嗜雪的蚂蚁
 **/
@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/minio")
@Api(value = "minio文件服务器Controller",tags = "minio文件服务")
public class MinioController {

    private final MinioHandler minioHandler;
    private final MinioProperties minioProperties;

    /**
     * 创建存储桶
     */
    @GetMapping("/createdBucket")
    @ApiOperation("创建存储桶")
    public Result createdBucket(String bucketName) throws Exception {
        boolean result = minioHandler.createdBucket(bucketName);
        return result? Result.ok("创建成功"):Result.failed("创建失败，请重试");
    }

    /**
     * 删除存储桶
     */
    @GetMapping("/removeBucket")
    @ApiOperation("删除存储桶")
    public Result removeBucket(String bucketName) {
        boolean result = minioHandler.removeBucket(bucketName);
        return result?Result.ok("删除成功"):Result.failed("删除失败，请重试");
    }

    /**
     * 上传文件
     */
    @PostMapping("/upload")
    @ApiOperation("上传文件")
    public Result saveFile(MultipartFile[] files) throws Exception {
        List<String> uploadFileList = minioHandler.saveFile(files);
        return Result.ok(uploadFileList);
    }

    /**
     * 下载文件
     */
    @GetMapping(value = "/download")
    @ApiOperation("下载文件")
    public ResponseEntity<byte[]> download(String filename) {
        return minioHandler.download(filename);
    }

    /**
     * 以图片形式返回
     */
    @GetMapping("/get/image")
    @ApiOperation("以图片形式返回")
    public ResponseEntity<byte[]> getImage(String filename) {
        return minioHandler.download(filename, MediaType.IMAGE_JPEG);
    }

    /***
     * 获取已上传文件的访问URL
     *
     * 这里从配置中指定的默认bucket中获取
     **/
    @GetMapping("/getPresignedUrl")
    @ApiOperation("获取访问URL")
    public Result getPresignedUrl(String fileName,Integer expire){
        String presignedObjectUrl="";
        try {
             presignedObjectUrl = minioHandler.getPresignedObjectUrl(minioProperties.getBucketName(), fileName, expire);
        } catch (Exception e) {
            String errMsg=String.format("/minio/getPresignedUrl 获取访问URL 异常:bucketName=%s,fileName=%s,expire=%s",
                    minioProperties.getBucketName(),fileName,expire);
            log.error(errMsg);
            return Result.failed(errMsg);
        }
        return Result.ok(presignedObjectUrl);
    }

    /***
     * 指定bucket获取已上传文件的访问URL
     *
     **/
    @GetMapping("/getPresignedUrlWithBucket")
    @ApiOperation("从指定bucket获取访问URL")
    public Result getPresignedUrlWithBucket(String bucketName,String fileName,Integer expire){
        String presignedObjectUrl="";
        try {
            presignedObjectUrl = minioHandler.getPresignedObjectUrl(bucketName, fileName, expire);
        } catch (Exception e) {
            String errMsg=String.format("/minio/uploadUrl 上传文件并获取访问URL异常:bucketName=%s,fileName=%s,expire=%s",bucketName,fileName,expire);
            log.error(errMsg);
            return Result.failed(errMsg);
        }
        return Result.ok(presignedObjectUrl);
    }
}
