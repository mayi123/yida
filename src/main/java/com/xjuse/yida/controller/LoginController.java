package com.xjuse.yida.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xjuse.yida.commom.ConstantEnum;
import com.xjuse.yida.commom.jwt.JwtUser;
import com.xjuse.yida.commom.jwt.PasswordEncoder;
import com.xjuse.yida.commom.jwt.TokenProvider;
import com.xjuse.yida.commom.util.Result;
import com.xjuse.yida.entity.UserInfo;
import com.xjuse.yida.mapper.UserInfoMapper;
import com.xjuse.yida.service.UserInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/***
 * @Description  用户登录注册  控制器
 * @Date 13:17 2022/10/22
 * @author 嗜雪的蚂蚁
 **/
@RestController
@RequiredArgsConstructor
@Api(value = "用户登录注册控制器",tags = "用户登录注册")
public class LoginController {

    private final UserInfoService userInfoService;
    private final UserInfoMapper userInfoMapper;

    @PostMapping("/register")
    @ApiOperation("(普通)用户注册")
    public Result register(@RequestParam("username") String username,
                                   @RequestParam("password") String password,
                                   @RequestParam("sex") String sex) {
       return userInfoService.register(username,password,sex,2);
    }

    @PostMapping("/registerAdmin")
    @ApiOperation("(管理员)用户注册")
    public Result registerAdmin(@RequestParam("username") String username,
                           @RequestParam("password") String password,
                           @RequestParam("sex") String sex) {
        return userInfoService.register(username,password,sex,1);
    }

    @GetMapping("/login")
    @ApiOperation("用户登录")
    public Result login(String username, String password, HttpServletRequest request) {
        UserInfo currUser = userInfoMapper.getUserByName(username);
        if (currUser==null||StringUtils.isEmpty(currUser.getUserId())){
            return Result.failed("用户名信息不存在，请先注册!");
        }
        if (PasswordEncoder.md5Matches(password, currUser.getUserPwd())) {
            String token = TokenProvider.createToken(currUser.getUserId(), "app", String.valueOf(currUser.getRoleId()));
            //成功生成token后进行定位和天气信息的处理
            userInfoService.handlerAfterLogin(token,request);
            return Result.ok(token);
        }
        return Result.error("用户名或密码错误，验证失败!");
    }

    @GetMapping("/token/validate")
    @ApiOperation("token校验")
    public Result tokenValidate(String token) {
        JwtUser jwtUser = TokenProvider.checkToken(token);
        return Result.ok(jwtUser);
    }

    @PostMapping("/completeUser")
    @ApiOperation("完善用户信息")
    public Result completeUserInfo(UserInfo userInfo){
        if (userInfo==null||StringUtils.isEmpty(userInfo.getUserId())){
            return Result.failed("用户信息不能为空!");
        }
        String userId = userInfo.getUserId();
        boolean exists = userInfoMapper.exists(new QueryWrapper<UserInfo>().eq("user_id", userId));
        if (exists){
            userInfoService.updateUser(userInfo);
            UserInfo newUserInfo = userInfoMapper.selectById(userId);
            return Result.ok(newUserInfo);
        }
        return Result.failed("用户信息不存在，更新失败!");
    }

}
