package com.xjuse.yida.controller;


import com.xjuse.yida.commom.jwt.JwtUser;
import com.xjuse.yida.commom.jwt.interceptor.AuthStorage;
import com.xjuse.yida.commom.util.Result;
import com.xjuse.yida.dto.UserClothRecomDto;
import com.xjuse.yida.entity.UserClothes;
import com.xjuse.yida.entity.WearRecommand;
import com.xjuse.yida.service.WearRecommandService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 衣物穿搭推荐 前端控制器
 * </p>
 *
 * @author duwx
 * @since 2022-10-21
 */
@RestController
@RequestMapping("/wear-recommand")
@RequiredArgsConstructor
@Api(value = "用户衣物穿搭推荐",tags = "用户衣物穿搭推荐")
public class WearRecommandController {

    @Autowired
    private WearRecommandService wearRecommandService;

    @GetMapping("/userRecom")
    @ApiOperation("获取用户当天穿搭推荐")
    public Result userClothesRecom(){
        JwtUser user = AuthStorage.getUser();
        if (user==null|| StringUtils.isEmpty(user.getUserId())){
            return Result.failed("请先登录!");
        }
        String userId = user.getUserId();
        return wearRecommandService.createUserClothRecommend(userId);
    }

    @GetMapping("/recomHistory")
    @ApiOperation("用户穿搭推荐记录查询")
    public Result getUserClothRecomHistory(){
        JwtUser user = AuthStorage.getUser();
        if (user==null|| StringUtils.isEmpty(user.getUserId())){
            return Result.failed("请先登录!");
        }
        String userId = user.getUserId();
        List<WearRecommand> userClothRecomHistory = wearRecommandService.getUserClothRecomHistory(userId);
        return Result.ok(userClothRecomHistory);
    }

    @PostMapping("/cleanFreqSet")
    @ApiOperation("用户衣物清洁频率修改")
    public Result setUserCleanFreq(int freq){
        JwtUser user = AuthStorage.getUser();
        if (user==null|| StringUtils.isEmpty(user.getUserId())){
            return Result.failed("请先登录!");
        }
        String userId = user.getUserId();
        boolean result = wearRecommandService.setUserCleanFreq(userId,freq);
        return result?Result.ok("清洁频率修改成功!"):Result.failed("清洁频率修改失败!");
    }

    @GetMapping("/userTodayCleans")
    @ApiOperation("查询用户当天的清洁计划")
    public Result queryUserTodayCleans(){
        JwtUser user = AuthStorage.getUser();
        if (user==null|| StringUtils.isEmpty(user.getUserId())){
            return Result.failed("请先登录!");
        }
        String userId = user.getUserId();
        List<UserClothes> userClothes = wearRecommandService.queryUserTodayClean(userId);
        return Result.ok(userClothes);
    }

    @GetMapping("/userCleanHistory")
    @ApiOperation("获取用户清洁信息历史")
    public Result queryuserCleanHistory(){
        JwtUser user = AuthStorage.getUser();
        if (user==null|| StringUtils.isEmpty(user.getUserId())){
            return Result.failed("请先登录!");
        }
        String userId = user.getUserId();
        Map<String, List<UserClothes>> map = wearRecommandService.userClothesCleanHistory(userId);
        return Result.ok(map);
    }

}

