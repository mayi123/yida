package com.xjuse.yida.controller;

import com.alibaba.fastjson.JSONObject;
import com.xjuse.yida.commom.gaode.GaodeIpServiceUtil;
import com.xjuse.yida.commom.gaode.GaodeWeatherServiceUtil;
import com.xjuse.yida.commom.util.NetworkUtils;
import com.xjuse.yida.commom.util.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;

/**
 * @Desc 高德服务 控制器
 * @Author 嗜雪的蚂蚁
 * @Date 2022/10/25 2:27
 **/
@RestController
@RequiredArgsConstructor
@RequestMapping("/gaode")
@Api(value = "高德开放平台服务", tags = "高德开放平台服务")
public class GaodeServerController {

    @Autowired
    private GaodeIpServiceUtil gaodeIpServiceUtil;
    @Autowired
    private GaodeWeatherServiceUtil gaodeWeatherServiceUtil;

    @GetMapping("/location")
    @ApiOperation("获取当前定位信息")
    public Result getLocationByIp(HttpServletRequest request) {
        String userIp = NetworkUtils.getIpAddr(request);
        //todo 无法获取到准确的公网IP地址
//        userIp = "42.93.156.170";
        LinkedHashMap userLocation = gaodeIpServiceUtil.getUserLocation(userIp);
        return Result.ok(userLocation);
    }

    @GetMapping("/currentWeather")
    @ApiOperation("获取当前的天气信息")
    public Result getCurrentWeather(String adcode) {
        JSONObject current = gaodeWeatherServiceUtil.currentWeatherByAdcode(adcode);
        return Result.ok(current);
    }

    @GetMapping("/futureWeather")
    @ApiOperation("获取天气预报信息")
    public Result getFutureWeather(String adcode) {
        JSONObject future = gaodeWeatherServiceUtil.futureWeatherByAdcode(adcode);
        return Result.ok(future);
    }
}
