package com.xjuse.yida.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  定位和天气预报服务    前端控制器
 * </p>
 *
 * @author duwx
 * @since 2022-10-28
 */
@RestController
@RequestMapping("/location-weather")
public class LocationWeatherController {

}

