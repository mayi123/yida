package com.xjuse.yida.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xjuse.yida.commom.util.Result;
import com.xjuse.yida.entity.Cloth;
import com.xjuse.yida.service.ClothesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 衣物信息管理 前端控制器
 * </p>
 *
 * @author duwx
 * @since 2022-10-21
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/clothes")
@Api(value = "衣物信息管理控制器",tags = "衣物信息管理")
public class ClothesController {

    private final ClothesService clothesService;

    @GetMapping("/list")
    @ApiOperation("获取所有衣物信息分页列表")
    public Result queryClothesPageList(Page<Cloth> page) {
        IPage<Cloth> clothesIPage = clothesService.queryPageList(page);
        return Result.ok(clothesIPage);
    }

    @PostMapping("/addClothes")
    @ApiOperation("添加衣物类别")
    public Result addClothes(Cloth cloth) {
        boolean b = clothesService.insertClothes(cloth);
        return b ? Result.ok("录入成功!") : Result.failed("录入失败，请重试!");
    }
}

