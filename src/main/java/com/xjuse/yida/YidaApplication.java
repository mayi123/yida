package com.xjuse.yida;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = "com.xjuse.yida")
@EnableSwagger2
public class YidaApplication {

	public static void main(String[] args) {
		SpringApplication.run(YidaApplication.class, args);
	}

}
