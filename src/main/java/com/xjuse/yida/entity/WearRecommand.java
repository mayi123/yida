package com.xjuse.yida.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * 穿搭推荐实体类
 *
 * @author duwx
 * @since 2022-10-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Accessors(chain = true)
public class WearRecommand implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 衣物ID
     */
    private String clothId;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 推荐ID
     */
    private String recomId;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 衣物类型/衣物名称
     */
    private String clothType;

    /**
     * 推荐时间
     */
    private LocalDateTime recomTime;


}
