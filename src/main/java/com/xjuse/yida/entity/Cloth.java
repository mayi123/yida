package com.xjuse.yida.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import lombok.*;
import lombok.experimental.Accessors;

/**
 *  衣物信息实体类
 *
 * @author duwx
 * @since 2022-10-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Accessors(chain = true)
public class Cloth implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 衣物ID
     */
    @TableId(value = "cloth_id", type = IdType.ASSIGN_UUID)
    private String clothId;

    /**
     * 衣物类型/名称
     */
    private String clothName;

    /**
     * 衣物分类
     * 穿搭推荐时  每套推荐 每种type的衣服至多一件
     */
    private String clothType;

    /***
     * 衣物编码
     **/
    private String clothCode;

    /**
     * 适用天气
     */
    private String clothWeather;

    /**
     * 适用季节
     */
    private String clothSeason;

    /***
     * 衣物描述
     **/
    private String clothDesc;

}
