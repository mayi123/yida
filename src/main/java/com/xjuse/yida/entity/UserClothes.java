package com.xjuse.yida.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * 用户-衣物实体类
 *
 * @author duwx
 * @since 2022-10-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Accessors(chain = true)
public class UserClothes implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 衣物ID
     */
    private String clothId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 衣物类型/名称
     */
    private String clothType;

    /**
     * 衣物颜色
     */
    private String clothColour;

    /****
     * 衣物图片URL
     **/
    private String clothIconUrl;

    /**
     * 衣物保暖度
     */
    private Integer clothWarm;

    /**
     * 衣物风格
     */
    private String clothStyle;

    /**
     * 衣物录入时间
     */
    private LocalDateTime addTime;


}
