package com.xjuse.yida.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 *  用户信息  实体类
 * </p>
 *
 * @author duwx
 * @since 2022-10-22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Builder
public class UserInfo implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 用户ID
     */
    @TableId(value = "user_id", type = IdType.ASSIGN_UUID)
    private String userId;

    /**
     * 角色Id
     */
    private Integer roleId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 性别
     */
    private Boolean sex;

    /**
     * 用户密码
     */
    private String userPwd;

    /**
     * 手机号码
     */
    private String phoneNum;

    /**
     * 邮箱号码
     */
    private String email;

    /**
     * 注册时间
     */
    private LocalDateTime registerTime;

    /**
     * 上一次登陆时间
     */
    private LocalDateTime lastLoginTime;

    /**
     * 是否有效
     */
    private Boolean isValid;


}
