package com.xjuse.yida.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Date;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 * 定位和天气预报 实体类
 * </p>
 *
 * @author duwx
 * @since 2022-10-28
 */
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LocationWeather implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键   策略：自增
     */
    @TableId(value = "location_weath_id", type = IdType.AUTO)
    private Integer locationWeathId;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 定位-省
     */
    private String locationProvince;

    /**
     * 定位-县
     */
    private String locationCity;

    /**
     * 定位-城市编码
     */
    private String locationAdcode;

    /**
     * 天气现象（汉字描述）
     */
    private String weath;

    /**
     * 风力级别，单位：级
     */
    private String weathWindPower;

    /**
     * 风向描述
     */
    private String weathWindDirection;

    /**
     * 实时气温，单位：摄氏度
     */
    private String weathTemperature;

    /**
     * 空气湿度
     */
    private String weathHumidity;

    /**
     * 数据发布的时间
     */
    private String weathReportTime;

    /***
     * 数据记录日期
     **/
    private String recordDate;

    /**
     * 用户心情
     */
    private String userMood;
}
