package com.xjuse.yida.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xjuse.yida.entity.LocationWeather;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * <p>
 *  定位和天气信息   Mapper 接口
 * </p>
 *
 * @author duwx
 * @since 2022-10-28
 */
@Repository
public interface LocationWeatherMapper extends BaseMapper<LocationWeather> {

    /***
     *  根据用户Id 和日期信息 查询记录
     **/
    LocationWeather findByUserIdAndDate(String userId, String date);
}
