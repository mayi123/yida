package com.xjuse.yida.mapper;

import com.xjuse.yida.entity.WearRecommand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 穿搭推荐 Mapper 接口
 * </p>
 *
 * @author duwx
 * @since 2022-10-21
 */
@Repository
public interface WearRecommandMapper extends BaseMapper<WearRecommand> {

}
