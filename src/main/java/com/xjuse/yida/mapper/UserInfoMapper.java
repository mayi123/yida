package com.xjuse.yida.mapper;

import com.xjuse.yida.entity.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * �洢�û���Ϣ Mapper 接口
 * </p>
 *
 * @author duwx
 * @since 2022-10-22
 */
@Repository
public interface UserInfoMapper extends BaseMapper<UserInfo> {

    UserInfo getUserByName(@Param("userName") String userName);

}
