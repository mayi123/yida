package com.xjuse.yida.mapper;

import com.xjuse.yida.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author duwx
 * @since 2022-10-22
 */
@Repository
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
