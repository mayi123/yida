package com.xjuse.yida.mapper;

import com.xjuse.yida.entity.Cloth;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 衣物信息 Mapper 接口
 * </p>
 *
 * @author duwx
 * @since 2022-10-21
 */
@Repository
public interface ClothesMapper extends BaseMapper<Cloth> {

}
