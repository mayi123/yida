package com.xjuse.yida.service;

import com.xjuse.yida.commom.util.Result;
import com.xjuse.yida.entity.UserClothes;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 用户衣物服务类
 *
 * @author duwx
 * @since 2022-10-21
 */
public interface UserClothesService extends IService<UserClothes> {

    Result insertUserClothes(UserClothes userClothes);

    Result findClothesByUser();

    /**
     * 按 穿搭推荐分类  获取用户衣物
     * @return
     */
    Map<String,List<UserClothes>> clothByRecomType(String userId);

    /**
     * 通过用户ID和衣物ID集合 查寻衣物信息
     * @param userId 用户ID
     * @param clothesIds 衣物ID集合
     * @return 用户衣物列表
     */
    List<UserClothes> clothesByUserAndClothIds(String userId, String clothesIds);

}
