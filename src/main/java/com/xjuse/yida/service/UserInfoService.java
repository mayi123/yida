package com.xjuse.yida.service;

import com.xjuse.yida.commom.util.Result;
import com.xjuse.yida.entity.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 用户信息 服务类
 * </p>
 *
 * @author duwx
 * @since 2022-10-22
 */
public interface UserInfoService extends IService<UserInfo> {

    public boolean insertUser(UserInfo userInfo);

    public boolean updateUser(UserInfo userInfo);

    public List<UserInfo> queryUserList();

    public boolean deleteUser(String userId);

    /***
     * @Description  逻辑删除用户，置为无效
     * @return boolean
     **/
    public boolean deleteUserLogical(String userId);

    /***
     * 用户注册部分数据组装
     **/
    Result register(String username, String password, String sex, int roleId);

    /***
     * 用户登录后的后续处理
     **/
    void handlerAfterLogin(String token, HttpServletRequest request);

}
