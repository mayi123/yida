package com.xjuse.yida.service.impl;

import com.xjuse.yida.entity.UserRole;
import com.xjuse.yida.mapper.UserRoleMapper;
import com.xjuse.yida.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author duwx
 * @since 2022-10-22
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
