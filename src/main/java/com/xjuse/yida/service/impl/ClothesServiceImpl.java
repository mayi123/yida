package com.xjuse.yida.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xjuse.yida.entity.Cloth;
import com.xjuse.yida.mapper.ClothesMapper;
import com.xjuse.yida.service.ClothesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 衣物信息维护 服务实现类
 *
 * @author duwx
 * @since 2022-10-212
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ClothesServiceImpl extends ServiceImpl<ClothesMapper, Cloth> implements ClothesService {

    @Autowired
    private ClothesMapper clothesMapper;

    @Override
    public boolean insertClothes(Cloth cloth) {
        int result = clothesMapper.insert(cloth);
        return result > 0;
    }

    @Override
    public boolean insertBatch(List<Cloth> clothList) {
        return this.saveBatch(clothList);
    }

    @Override
    public boolean updateClothes(Cloth cloth) {
        int result = clothesMapper.updateById(cloth);
        return result > 0;
    }

    @Override
    public List<Cloth> queryClothesList() {
        return clothesMapper.selectList(new QueryWrapper<>());
    }

    @Override
    public IPage<Cloth> queryPageList(Page<Cloth> page) {
        return clothesMapper.selectPage(page, new QueryWrapper<>());
    }

    @Override
    public boolean deleteClothes(String clothId) {
        int result = clothesMapper.deleteById(clothId);
        return result > 0;
    }
}
