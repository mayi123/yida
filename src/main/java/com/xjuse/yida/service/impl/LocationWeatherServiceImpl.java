package com.xjuse.yida.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xjuse.yida.commom.gaode.GaodeIpServiceUtil;
import com.xjuse.yida.commom.gaode.GaodeWeatherServiceUtil;
import com.xjuse.yida.commom.util.TimeUtil;
import com.xjuse.yida.entity.LocationWeather;
import com.xjuse.yida.entity.UserInfo;
import com.xjuse.yida.mapper.LocationWeatherMapper;
import com.xjuse.yida.service.LocationWeatherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.LinkedHashMap;

/**
 * <p>
 * 天气和定位信息   服务实现类
 * </p>
 *
 * @author duwx
 * @since 2022-10-28
 */
@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class LocationWeatherServiceImpl extends ServiceImpl<LocationWeatherMapper, LocationWeather> implements LocationWeatherService {

    @Autowired
    private LocationWeatherMapper locationWeatherMapper;
    @Autowired
    private GaodeIpServiceUtil gaodeIpServiceUtil;
    @Autowired
    private GaodeWeatherServiceUtil gaodeWeatherServiceUtil;

    /***
     * @Description 添加用户定位信息
     * @author 嗜雪的蚂蚁
     **/
    @Override
    public boolean insertLocation(UserInfo user,String ip) {
        String currentUserId = user.getUserId();
        String currentDate = TimeUtil.currentDateString();
        LinkedHashMap<String, Object> locationMap = gaodeIpServiceUtil.getUserLocation(ip);
        //使用已知数据构造 实体
        LocationWeather location = LocationWeather.builder()
                .locationProvince(String.valueOf(locationMap.get("province")))
                .locationCity(String.valueOf(locationMap.get("city")))
                .locationAdcode(String.valueOf(locationMap.get("adcode")))
                .recordDate(currentDate)
                .userId(currentUserId)
                .userName(user.getUserName())
                .build();
        //先查询当天时间是否已有定位信息  若有 则需要对其进行更新操作
        LocationWeather byUserIdAndDate = locationWeatherMapper.findByUserIdAndDate(currentUserId, currentDate);
        int result;
        if (byUserIdAndDate==null|| StringUtils.isEmpty(byUserIdAndDate.getLocationWeathId())){
            //当天第一次定位  直接创建记录
            result = locationWeatherMapper.insert(location);
        }else {
            location.setLocationWeathId(byUserIdAndDate.getLocationWeathId());
            result = locationWeatherMapper.updateById(location);
        }
        return result>0;
    }

    @Override
    public LocationWeather findUserCurrLocationWeather(String userId) {
        String currentDate = TimeUtil.currentDateString();
        return  locationWeatherMapper.findByUserIdAndDate(userId, currentDate);
    }

    @Override
    public boolean insertWeather(UserInfo user) {
        String currentUserId = user.getUserId();
        String currentDate = TimeUtil.currentDateString();
        //查询已有的定位信息记录   该方法总是根据已有的定位信息来获取天气信息
        LocationWeather locationInfo = locationWeatherMapper.findByUserIdAndDate(currentUserId, currentDate);
        if (locationInfo==null||StringUtils.isEmpty(locationInfo.getLocationWeathId())){
            log.error("LocationWeatherServiceImpl--insertWeather未获取到已有的定位信息: userInfo={},date={}",user.toString(),currentDate);
            return false;
        }
        //查询到定位信息后根据adCode查询天气信息
        String adCode = locationInfo.getLocationAdcode();
        JSONObject weather = this.queryWeatherByAdCode(adCode);

        locationInfo.setWeath(weather.getString("weather"));
        locationInfo.setWeathWindPower(weather.getString("windpower"));
        locationInfo.setWeathTemperature(weather.getString("temperature"));
        locationInfo.setWeathWindDirection(weather.getString("winddirection"));
        locationInfo.setWeathHumidity(weather.getString("humidity"));
        locationInfo.setWeathReportTime(weather.getString("reporttime"));

        int result = locationWeatherMapper.updateById(locationInfo);
        return result>1;
    }

    /***
     * @Description 添加用户天气信息
     * @author 嗜雪的蚂蚁
     **/
    @Override
    public JSONObject queryWeatherByAdCode(String adCode) {
        return gaodeWeatherServiceUtil.currentWeatherByAdcode(adCode);
    }
}
