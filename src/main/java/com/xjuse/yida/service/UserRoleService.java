package com.xjuse.yida.service;

import com.xjuse.yida.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author duwx
 * @since 2022-10-22
 */
public interface UserRoleService extends IService<UserRole> {

}
