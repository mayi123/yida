package com.xjuse.yida.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xjuse.yida.entity.LocationWeather;
import com.xjuse.yida.entity.UserInfo;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  天气和定位信息   服务类
 * </p>
 *
 * @author duwx
 * @since 2022-10-28
 */
public interface LocationWeatherService extends IService<LocationWeather> {


    public boolean insertLocation(UserInfo user,String ip);

    public JSONObject queryWeatherByAdCode(String adCode);

    public boolean insertWeather(UserInfo user);

    /**
     * 获取用户当天的天气和定位信息实体
     * @param userId 用户ID
     * @return
     */
    public LocationWeather findUserCurrLocationWeather(String userId);
}
