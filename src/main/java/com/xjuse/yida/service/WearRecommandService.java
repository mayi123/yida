package com.xjuse.yida.service;

import com.alibaba.fastjson.JSONObject;
import com.xjuse.yida.commom.util.Result;
import com.xjuse.yida.dto.UserClothRecomDto;
import com.xjuse.yida.entity.UserClothes;
import com.xjuse.yida.entity.WearRecommand;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 穿搭推荐服务类
 *
 * @author duwx
 * @since 2022-10-21
 */
public interface WearRecommandService extends IService<WearRecommand> {


    /**
     *创建用户穿搭推荐
     */
    public Result createUserClothRecommend(String userId);

    /**
     * 查询用户穿搭推荐历史
     */
    public List<WearRecommand> getUserClothRecomHistory(String userId);

    /**
     * 设置用户衣物清洁提醒频率
     * @param userId 用户ID
     * @return 成功与否
     */
    boolean setUserCleanFreq(String userId,int freq);

    /**
     * 生成当前用户需要清理的衣物数据   并缓存
     * @param userId 用户ID
     * @return 需要清理的用户衣物信息列表
     */
    List<UserClothes> needCleanUserClothesList(String userId);

    /**
     * 查询衣物信息清洁历史
     * @param userId 用户ID
     * @return 衣物信息列表
     */
    Map<String,List<UserClothes>> userClothesCleanHistory(String userId);

    /**
     * 获取用户当天衣物清洁信息
     * @param userId 用户ID
     * @return 衣物信息列表
     */
    List<UserClothes> queryUserTodayClean(String userId);
}
