package com.xjuse.yida.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xjuse.yida.entity.Cloth;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 衣物信息服务类
 * @author duwx
 * @since 2022-10-21
 */
public interface ClothesService extends IService<Cloth> {

    boolean insertClothes(Cloth cloth);

    boolean insertBatch(List<Cloth> clothList);

    public boolean updateClothes(Cloth cloth);

    public List<Cloth> queryClothesList();

    public IPage<Cloth> queryPageList(Page<Cloth> page);

    public boolean deleteClothes(String clothId);

}
