package com.xjuse.yida.commom;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/***
 * @Description 系统常量参数枚举类
 * @Date 14:50 2022/10/22
 * @author 嗜雪的蚂蚁
 **/
@AllArgsConstructor
@Getter
@ToString
public enum ConstantEnum {


    //性别
    SEX_MALE("男", true,"0001"),
    SEX_FEMALE("女",false,"0001"),
    //通用 是否有效
    VALID("有效", true,"0002"),
    INVALID("无效",false,"0002"),

    //衣物类别
    COAT("外套","0003001","0006001"),
    SHORT_SLEEVE("短袖","0003002","0006001"),
    T_SHIRT("T恤","0003003","0006001"),
    SHORTS("短裤","0003004","0006002"),
    DRESS("裙子","0003005","0006002"),
    TROUSERS("长裤","0003006","0006002"),
    DOWN_COAT("羽绒服","0003007","0006001"),
    SHOE("鞋子","0003008","0006003"),
    ORNAMENT("包包/手表","0003009","0006004"),

    //衣物穿搭类别
    SHANGSHENG("上身","0006001","0006"),
    XIASHENG("下身","0006002","0006"),
    SHOES("鞋子","0006003","0006"),
    OTHER("其他","0006004","0006"),

    //季节
    SPRING("春","0004001","0004"),
    SUMMER("夏","0004002","0004"),
    AUTUMN("秋","0004003","0004"),
    WINTER("冬","0004004","0004"),

    //用户心情
    HAPPY("开心","0005001","0005"),
    GENERAL("一般","0005002","0005"),
    UNHAPPY("不开心","0005003","0005"),

    //Redis key前缀
    USER_CLEAN_FREQ_KEY_PREFIX("用户衣物清洁频率缓存key前缀","user_clean_frep_key",null),
    USER_CLOTHES_FREQ_KEY_PREFIX("用户衣物搭配次数缓存key前缀","user_clothes_freq_key",null),
    USER_CLEAN_CLOTH_SET("用户每天清洁的衣物列表缓存key","user_clean_cloth_key",null);

    //对应文字 用于传输响应时
    private final String Str;
    //数据  用于数据库存储
    private final Object data;
    //分类编码
    private final String code;


    /***
     * @Description  根据String值获取数据库值
     **/
    public static ConstantEnum getEnumByStr(String str){
        return Arrays.stream(ConstantEnum.values())
                .filter(curr -> curr.getStr().equals(str))
                .collect(Collectors.toList()).get(0);
    }

    /***
     * @Description  根据data获取数据库值
     **/
    public static ConstantEnum getEnumByData(String data){
        return Arrays.stream(ConstantEnum.values())
                .filter(curr -> data.equals(curr.getData()))
                .collect(Collectors.toList()).get(0);
    }

    /***
     * @Description  获取同一code的所有枚举类信息
     * @author 嗜雪的蚂蚁
     **/
    public static List<ConstantEnum> getEnumsByCode(String code){
        return Arrays.stream(ConstantEnum.values())
                .filter(curr -> code.equals(curr.getCode()))
                .collect(Collectors.toList());
    }
}
