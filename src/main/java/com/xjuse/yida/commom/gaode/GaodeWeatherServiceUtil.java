package com.xjuse.yida.commom.gaode;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * @Desc 高德天气查询服务
 * @Author 嗜雪的蚂蚁
 * @Date 2022/10/25 17:19
 **/
@Component
@Slf4j
public class GaodeWeatherServiceUtil {

    private static final String appKey = "9ecbd556fed16b621ee86dc931d94fe8";
    public final String WEATHER_URL = "https://restapi.amap.com/v3/weather/weatherInfo?";

    /***
     * @Description 通过adCode 获取当前天气
     * @author 嗜雪的蚂蚁
     **/
    public JSONObject currentWeatherByAdcode(String adcode) {
        HashMap<String, String> params = new HashMap<>();
        params.put("adcode", adcode);
        return this.getCurrent(params);
    }

    /***
     * @Description 通过adCode 获取未来天气预报
     * @author 嗜雪的蚂蚁
     **/
    public JSONObject futureWeatherByAdcode(String adcode) {
        HashMap<String, String> params = new HashMap<>();
        params.put("adcode", adcode);
        return this.getFuture(params);
    }

    /**
     * 获取当前天气状况
     *
     * @return 天气信息JSON
     */
    public JSONObject getCurrent(Map<String, String> params) {
        JSONObject jsonObject = null;
        CloseableHttpClient httpclient = HttpClients.createDefault();
        // 创建URI对象，并且设置请求参数
        try {
            URI uri = getBuilderCurrent(WEATHER_URL, params);
            // 创建http GET请求
            HttpGet httpGet = new HttpGet(uri);
            CloseableHttpResponse response = httpclient.execute(httpGet);

            // 判断返回状态是否为200
            jsonObject = getRouteCurrent(response);
            httpclient.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    /**
     * 查询未来的
     * 发送get请求
     *
     * @return
     */
    public JSONObject getFuture(Map<String, String> params) {
        JSONObject jsonObject = null;
        CloseableHttpClient httpclient = HttpClients.createDefault();
        // 创建URI对象，并且设置请求参数
        try {
            URI uri = getBuilderFuture(WEATHER_URL, params);
            // 创建http GET请求
            HttpGet httpGet = new HttpGet(uri);
            CloseableHttpResponse response = httpclient.execute(httpGet);

            // 判断返回状态是否为200
            jsonObject = getRouteFuture(response);
            httpclient.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    /**
     * 根据不同的路径规划获取距离
     *
     * @param response
     * @return
     */
    private static JSONObject getRouteCurrent(CloseableHttpResponse response) throws Exception {
        JSONObject live = null;
        // 判断返回状态是否为200
        if (response.getStatusLine().getStatusCode() == 200) {
            String content = EntityUtils.toString(response.getEntity(), "UTF-8");
            log.info("调用高德地图接口返回的结果为:{}", content);
            JSONObject jsonObject = (JSONObject) JSONObject.parse(content);
            JSONArray lives = (JSONArray) jsonObject.get("lives");
            live = (JSONObject) lives.get(0);

            log.info("返回的结果为:{}", JSONObject.toJSONString(live));
        }
        return live;
    }

    /**
     * 封装请求的URI
     *
     * @param url    天气信息的高德开放平台的URL
     * @param params 参数信息
     * @return
     * @throws Exception
     */
    private URI getBuilderCurrent(String url, Map<String, String> params) throws Exception {
        // 城市编码，高德地图提供
        String adcode = params.get("adcode");

        URIBuilder uriBuilder = new URIBuilder(url);
        // 公共参数
        uriBuilder.setParameter("key", appKey);
        uriBuilder.setParameter("city", adcode);

        log.info("请求的参数key为:{}, cityCode为:{}", appKey, adcode);
        return uriBuilder.build();
    }

    /**
     * 封装URI
     *
     * @param url
     * @param params
     * @return
     * @throws Exception
     */
    private URI getBuilderFuture(String url, Map<String, String> params) throws Exception {
        // 城市编码，高德地图提供
        String adcode = params.get("adcode");

        URIBuilder uriBuilder = new URIBuilder(url);
        // 公共参数
        uriBuilder.setParameter("key", appKey);
        uriBuilder.setParameter("city", adcode);
        uriBuilder.setParameter("extensions", "all");

        log.info("请求的参数key为:{}, cityCode为:{}", appKey, adcode);
        URI uri = uriBuilder.build();
        return uri;
    }

    /**
     * 根据不同的路径规划获取距离
     *
     * @param response
     * @return
     */
    private static JSONObject getRouteFuture(CloseableHttpResponse response) throws Exception {
        JSONObject live = null;
        // 判断返回状态是否为200
        if (response.getStatusLine().getStatusCode() == 200) {
            String content = EntityUtils.toString(response.getEntity(), "UTF-8");
            log.info("调用高德地图接口返回的结果为:{}", content);
            JSONObject jsonObject = (JSONObject) JSONObject.parse(content);
            JSONArray forecast = (JSONArray) jsonObject.get("forecasts");
            live = (JSONObject) forecast.get(0);

            log.info("返回的结果为:{}", JSONObject.toJSONString(live));
        }
        return live;
    }


}
