//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.xjuse.yida.commom.util;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/***
 * @Description  响应结果实体类
 * @Date 18:15 2022/10/21
 * @author 嗜雪的蚂蚁
 **/
@ApiModel(value = "接口返回对象", description = "接口返回对象")
@Data
@AllArgsConstructor
@Builder
public class Result<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty("成功标志")
    private boolean success = true;
    @ApiModelProperty("返回处理消息")
    private String message = "操作成功！";
    @ApiModelProperty("返回处理消息")
    private String msg = "操作成功！";
    @ApiModelProperty("返回代码")
    private Integer code = 0;
    @ApiModelProperty("返回数据对象")
    private T result;
    @ApiModelProperty("时间戳")
    private long timestamp = System.currentTimeMillis();

    public Result() {
    }

    public Result<T> error500(String message) {
        this.message = message;
        this.msg = message;
        this.code = 500;
        this.success = false;
        return this;
    }

    public Result<T> success(String message) {
        this.message = message;
        this.msg = message;
        this.code = 200;
        this.success = true;
        return this;
    }

    public Result<T> ok() {
        this.setSuccess(true);
        this.setCode(200);
        this.setMessage("成功");
        this.setMsg("成功");
        return this;
    }

    public static Result<Object> ok(Object data,String msg) {
        Result<Object> r = new Result<>();
        r.setSuccess(true);
        r.setCode(200);
        r.setMessage("成功");
        r.setMsg(msg);
        r.setResult(data);
        return r;
    }

    public static Result<Object> ok(Object data) {
        Result<Object> r = new Result<>();
        r.setSuccess(true);
        r.setCode(200);
        r.setResult(data);
        return r;
    }

    public static Result<Object> error(String msg) {
        return error(500, msg);
    }

    public static Result failed(String msg) {
        return error(500, msg);
    }

    public static Result<Object> error(int code, String msg) {
        Result<Object> r = new Result();
        r.setCode(code);
        r.setMessage(msg);
        r.setMsg(msg);
        r.setSuccess(false);
        return r;
    }

    public boolean isSuccess() {
        return this.success;
    }

}
