package com.xjuse.yida.commom.util;


import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 时间工具类
 *
 * @author duwenxu
 * @create 2020-08-24 14:41
 */
public class TimeUtil {

    /**
     * 时间Format
     */
    public static final String Y_MS_NORMAL_40 = "yyyy-MM-dd HH:mm:ss.SSSS";
    public static final String Y_MS_NORMAL_30 = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String Y_S_CLOSE = "yyyyMMddHHmmss";
    public static final String Y_D_LINK = "yyyyMMdd";
    public static final String Y_D_NORMAL = "yyyy-MM-dd";
    public static final String Y_S_NORMAL = "yyyy-MM-dd HH:mm:ss";


    /**
     * 时长常量
     */
    public static final Long ONE_DAY_LENGTH = 86400000L;     //每天的毫秒数
    public static final Long EIGHT_HOURS_LENGTH = 28800000L;
    public static final Long THREE_DAY_LENGTH = 86400000 * 3L;
    public static final Long ONE_WEEK_LENGTH = 86400000 * 7L;
    public static final Long ONE_HOUR_LENGTH = 3600000L;
    public static final Long ONE_MINUTE_LENGTH = 60000L;

    public static final Long SECOND = 1000L;
    public static final Long MINUTE = 60L * SECOND;
    public static final Long HOUR = 60L * MINUTE;
    public static final Long DAY = 24L * HOUR;


    /**
     * 时间格式转换
     *
     * @param source
     * @param format1
     * @param format2
     * @return
     */
    public static String timeConvert(String source, String format1, String format2) {
        return DateTime.parse(source, DateTimeFormat.forPattern(format1)).toString(format2);
    }

    /**
     * 格式化给定时间戳
     *
     * @param timeStr
     * @param timePattern
     * @return
     */

    public static String getTimeStr(Long timeStr, String timePattern) {
        return new SimpleDateFormat(timePattern).format(timeStr);
    }

    /**
     * 获取指定格式的时间的 UTC
     *
     * @param timeStr       时间String
     * @param sourcePattern 源格式
     * @param targetPattern 目标格式
     * @return
     */
    public static String getUTC(String timeStr, String sourcePattern, String targetPattern) {
        long timeLength = DateTime.parse(timeStr, DateTimeFormat.forPattern(sourcePattern)).getMillis() + EIGHT_HOURS_LENGTH;
        return getTimeStr(timeLength, targetPattern);
    }

    /**
     * 替换时间格式的.后边为随机数
     *
     * @param doubleTime
     * @return
     */
    public static String nanoRandom(String doubleTime) {
        String suffix = doubleTime.split("\\.")[1];
        switch (suffix) {
            case "000":
                return doubleTime.replace(".000", String.valueOf(ThreadLocalRandom.current().nextInt(100, 999)));
            case "0000":
                return doubleTime.replace(".0000", String.valueOf(ThreadLocalRandom.current().nextInt(1000, 9999)));
            default:
                return doubleTime;
        }
    }

    /**
     * 时间转换   FIXME：待测试
     *
     * @return
     */
    public static String transTime(Long time) {
        long zeroTime = System.currentTimeMillis() / (1000 * 3600 * 24) * (1000 * 3600 * 24) + time;
        return new SimpleDateFormat(Y_S_NORMAL).format(new Date(zeroTime));
    }

    /**
     * 时间字符串转Long
     *
     * @param timeStr 格式化的时间String
     * @param pattern 时间格式化规则
     * @return
     */
    public static Long timeStr2Long(String timeStr, String pattern) {
        DateTime dateTime = DateTime.parse(timeStr, DateTimeFormat.forPattern(pattern));
        return dateTime.getMillis();
    }

    public static Long toTimestamp(LocalDateTime dateTime) {
        if(null == dateTime) {
            return null;
        }

        ZonedDateTime zonedDateTime = ZonedDateTime.of(dateTime, ZoneId.systemDefault());
        return zonedDateTime.toInstant().toEpochMilli();
    }

    public static String format(LocalDateTime time, String pattern) {
        if(null == time || null == pattern || "".equals(pattern)) {
            return null;
        }

        return time.format(DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * 获取毫秒值
     *
     * @param timeNs
     * @return
     */
    public static Character ws100(String timeNs) {
        return timeNs.toCharArray()[timeNs.length() - 6];
    }

    public static LocalDateTime fromLong(long timestamp) {
        Instant instant = Instant.ofEpochMilli(timestamp);
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
    }

    /***
     * @Description  获取当天的日期String
     * @author 嗜雪的蚂蚁
     **/
    public static String currentDateString(){
        return TimeUtil.format(LocalDateTime.now(),TimeUtil.Y_D_NORMAL);
    }

    /**
     * 获取当前季节
     * @return
     */
    public static String getCurrSeason(){
        int seasonNumber = Calendar.getInstance().get(Calendar.MONTH);
        return seasonNumber>=1&&seasonNumber<=3?"0004001":seasonNumber>=4&&seasonNumber<=6?"0004002":seasonNumber>=7&&seasonNumber<=9?"0004003":seasonNumber>=10?"0004004":"0004004";
    }
}
