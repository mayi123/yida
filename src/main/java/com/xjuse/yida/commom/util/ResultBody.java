package com.xjuse.yida.commom.util;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/***
 * @Description   接口结构体
 * @Date 18:14 2022/10/21
 * @author 嗜雪的蚂蚁
 **/

@ApiModel("响应结果")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResultBody<T> {

    @ApiModelProperty("响应编码:请求处理成功")
    private Integer code = 200;

    @ApiModelProperty("提示消息")
    private String message;

    @ApiModelProperty("响应数据")
    private T data;

    public static <T> ResultBody<T> success(){
        return new ResultBody<T>(200, "", null);
    }

    public static <T> ResultBody<T> success(T data){
        return new ResultBody<T>(200, "", data);
    }

    public static <T> ResultBody<T> error(){
        return new ResultBody<T>(500, "服务器内部错误", null);
    }

    public static <T> ResultBody<T> error(int code, String message){
        return new ResultBody<T>(code, message, null);
    }

    public static <T> ResultBody<T> build(int code, T data){
        return new ResultBody<T>(code, "", data);
    }

    public static <T> ResultBody<T> build(int code, String message, T data){
        return new ResultBody<T>(code, message, data);
    }
}
