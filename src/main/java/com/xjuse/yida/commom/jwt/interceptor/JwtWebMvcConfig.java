package com.xjuse.yida.commom.jwt.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/***
 * @Description  配置拦截器路径
 * @Date 17:05 2022/10/21
 * @author 嗜雪的蚂蚁
 **/
@Configuration
public class JwtWebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthInterceptor())
                // 拦截的路径   一般接口
                .addPathPatterns("/**")
                // 开放的路径
                .excludePathPatterns("/login/**", "/token/validate","/register/**");
    }
}
