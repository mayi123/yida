package com.xjuse.yida.commom.jwt;

import org.springframework.util.DigestUtils;

/***
 * @Description 密码加密工具类:对密码进行md5加密
 * @Date 16:28 2022/10/21
 * @author 嗜雪的蚂蚁
 **/
public class PasswordEncoder {

    /**
     * 密码加密
     * @param rawPassword 登录时传入的密码
     */
    public static String md5Encode(CharSequence rawPassword) {
        return DigestUtils.md5DigestAsHex(rawPassword.toString().getBytes());
    }

    /**
     * 密码对比
     * @param rawPassword 登录时传入的密码
     * @param encodedPassword 数据库保存的加密过的密码
     */
    public static boolean md5Matches(CharSequence rawPassword, String encodedPassword) {
        return encodedPassword.equals(DigestUtils.md5DigestAsHex(rawPassword.toString().getBytes()));
    }
}
