package com.xjuse.yida.commom.jwt;

import lombok.Data;
import lombok.experimental.Accessors;

/***
 * @Description 存储JWT用户的基本信息
 * @Date 16:25 2022/10/21
 * @author 嗜雪的蚂蚁
 **/
@Data
@Accessors(chain = true)
public class JwtUser {

    private boolean valid;
    private String userId;
    private String role;

    public JwtUser() {
        this.valid = false;
    }
}
